/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.login')
      .controller('LoginPageCtrl', LoginPageCtrl);
  /** @ngInject */
  function LoginPageCtrl($scope,$http,$cookies,$location) {
    var auth;
    $scope.login = {
      name: "",
      password: ""
    }
    $scope.onLogin = function(){
       console.log("Logging in .. ");
       
       $http({
          url: "https://sir.mediaprimalabs.com/api/admin_users/login",
          method: "POST",
          data: { "email": $scope.login.name,
                  "password": $scope.login.password },
      }).success(function (response) {
          console.log(response);
          $cookies.put('auth_token', response.data.auth_token);
          $scope.login = true;
          $location.path("/main");
      }).error(function (response) {
          console.log("error"+response);
      });
    };
    $scope.onLogout = function(){
      console.log("logout");
      $scope.login = false;
      $location.path("/login");
      $cookies.remove('auth_token');
    }
  }

})();
