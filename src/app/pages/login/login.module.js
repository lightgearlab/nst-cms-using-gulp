/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.login', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('login', {
          url: '/login',
          template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          controller: 'LoginPageCtrl',
          title: 'Login',
        }).state('login.main', {
          url: '/main',
          templateUrl: 'app/pages/login/loginModal.html',
          title: '',
          sidebarMeta: {
            order: 0,
          },
        });
    $urlRouterProvider.when('/login','/login/main');
  }

})();
