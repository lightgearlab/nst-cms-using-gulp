/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.main', [])
    .config(routeConfig)
    .factory('getfromServer', function ($http, $q){
              this.getStudent = function(auth_token){            
                  return $http.post('https://sir.mediaprimalabs.com/api/users/list_info',
                  { "auth_token": auth_token })
                  .then(function(response) {
                    //console.log(response); 
                    return response.data;
                  });            
              }
              this.getWords = function(auth_token){            
                  return $http.post('https://sir.mediaprimalabs.com/api/questions_list',
                  { "auth_token": auth_token })
                  .then(function(response) {
                    //console.log(response); 
                    return response.data.questions;
                  });            
              }
              this.getSponsor = function(){            
                  return $http.get('https://sir.mediaprimalabs.com/api/sponsors')
                  .then(function(response) {
                    //console.log(response); 
                    return response.data;
                  });            
              }
              return this;
    });
  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('main', {
          url: '/main',
          template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          controller: 'TablesPageCtrl',
          title: 'Main',
          sidebarMeta: {
            icon: 'ion-grid',
            order: 0,
          },
        }).state('main.student', {
          url: '/student',
          templateUrl: 'app/pages/main/student/tables.html',
          title: 'Student List',
          sidebarMeta: {
            order: 0,
          },
        }).state('main.basic', {
          url: '/basic',
          templateUrl: 'app/pages/main/basic/tables.html',
          title: 'Top Winner',
          sidebarMeta: {
            order: 1,
          },
        }).state('main.word', {
          url: '/word',
          templateUrl: 'app/pages/main/word/tables.html',
          title: 'Word List',
          sidebarMeta: {
            order: 2,
          },
        }).state('main.sponsor', {
          url: '/sponsor',
          templateUrl: 'app/pages/main/sponsor/tables.html',
          title: 'Sponsor List',
          sidebarMeta: {
            order: 3,
          },
        });
    $urlRouterProvider.when('/main','/main/student');
  }

})();
