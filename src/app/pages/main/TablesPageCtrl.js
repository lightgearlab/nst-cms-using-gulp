/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.main')
      .controller('TablesPageCtrl', TablesPageCtrl);
  /** @ngInject */
  function TablesPageCtrl($scope, $filter,$uibModal,$q,$http,$location,$cookies,getfromServer,$timeout,
  toastr, toastrConfig) {

    //setup for notification
    var defaultConfig = angular.copy(toastrConfig);
    $scope.types = ['success', 'error', 'info', 'warning'];
    var openedToasts = [];
    $scope.options = {
      autoDismiss: true,
      positionClass: 'toast-top-right',
      type: 'info',
      timeOut: '2000',
      extendedTimeOut: '2000',
      allowHtml: false,
      closeButton: false,
      tapToDismiss: true,
      progressBar: false,
      newestOnTop: true,
      maxOpened: 0,
      preventDuplicates: false,
      preventOpenDuplicates: false,
      title: "Some title here",
      msg: "Type your message here"
    };
    $scope.openToast = function () {
      angular.extend(toastrConfig, $scope.options);
      openedToasts.push(toastr[$scope.options.type]($scope.options.msg, $scope.options.title));
      var strOptions = {};
      for (var o in  $scope.options) if (o != 'msg' && o != 'title')strOptions[o] = $scope.options[o];
      $scope.optionsStr = "toastr." + $scope.options.type + "(\'" + $scope.options.msg + "\', \'" + $scope.options.title + "\', " + JSON.stringify(strOptions, null, 2) + ")";
    };
    $scope.dangerToast = function(title,msg) {
      $scope.options.title = title;
      $scope.options.type = 'error';
      $scope.options.msg = msg;
      $scope.openToast();
    };
    $scope.infoToast = function(title,msg) {
      $scope.options.title = title;
      $scope.options.type = 'info';
      $scope.options.msg = msg;
      $scope.openToast();
    };
    //end setup

    
    var auth;
    $scope.reloadFromServer = function() {
        auth = $cookies.get('auth_token');
        if(auth!=null){
          getfromServer.getStudent(auth).then(function(arrItems){
              $scope.students = arrItems;
              //console.log($scope.students);
              $scope.primary_pre = $scope.students.filter(function (app) {
                return (app.account_type == 'student' && app.education_type == 'primary');
              });
              $scope.primary_state = $scope.students.filter(function (app) {
                    return (app.account_type == 'state' && app.education_type == 'primary');
              });
              $scope.secondary_pre = $scope.students.filter(function (app) {
                    return (app.account_type == 'student' && app.education_type == 'secondary');
              });
              $scope.secondary_state = $scope.students.filter(function (app) {
                    return (app.account_type == 'state' && app.education_type == 'secondary');
              });
              $scope.allStudents = $scope.students;
              $scope.allStudents1 = $scope.students;

              getfromServer.getWords(auth).then(function(arrItems){
                // for(var i=0;0<arrItems.size;i++){
                //   arrItems.num = i;
                // }
                $scope.words = arrItems;
                //console.log($scope.words);
                $scope.allWords = $scope.words;
                $scope.allWords1 = $scope.words;


              });

              getfromServer.getSponsor().then(function(arrItems){
                $scope.sponsor = arrItems;
                $scope.allSponsor = $scope.sponsor;
                $scope.allSponsor1 = $scope.sponsor;
                //console.log($scope.sponsor);
              });
              //$scope.$apply();
          });
          
        }else{
          console.log("no auth");
          $location.path("/login");
        }
    };

    $scope.reloadFromServer();
    
    $scope.tablePageSize = 10;
    
    $scope.onCheckClick = function(){
      $scope.showOptionStudent = true;
      $scope.temp_val = $filter('filter')($scope.students, {selected:true});
      if($scope.temp_val.length == 0)
        $scope.showOptionStudent = false;
    }
    $scope.onCheckClickWord = function(){
      $scope.showOption = true;
      $scope.temp_val = $filter('filter')($scope.words, {selected:true});
      if($scope.temp_val.length == 0)
        $scope.showOption = false;
    }
    $scope.select_text = "Select All";
    $scope.selectall = function() {
      var toggleStatus = !$scope.isAllSelected;
      angular.forEach($scope.students, function(itm){ itm.selected = toggleStatus; });
      $scope.isAllSelected = !$scope.isAllSelected;
      $scope.showOptionStudent = toggleStatus;
      if($scope.isAllSelected){
        $scope.select_text = "Deselect All";
      }else{
        $scope.select_text = "Select All";
      }
    };
    $scope.selectallword = function() {
      var toggleStatus = !$scope.isAllSelected;
      angular.forEach($scope.words, function(itm){ itm.selected = toggleStatus; });
      $scope.isAllSelected = !$scope.isAllSelected;
      $scope.showOption = toggleStatus;
      if($scope.isAllSelected){
        $scope.select_text = "Deselect All";
      }else{
        $scope.select_text = "Select All";
      }
    };
    
    $scope.sendecertificate = function() {
      $scope.temp_val = $filter('filter')($scope.students, {selected:true});
      var name = "";
      for(var i=0;i < $scope.temp_val.length ; i++){
        name = $scope.temp_val[i].name +"\n"+ name;
      }
      openInfoModal("Send E-Certificate","Send E-Certificate to:\n"+name).then(function () {
        angular.forEach($scope.students, function(itm){ itm.selected = false; });
        $scope.showOptionStudent = false;
      });
    };
    $scope.selectstate = function() {
      $scope.temp_val = $filter('filter')($scope.students, {selected:true});
      var name = "",ic_array = "";
      for(var i=0;i < $scope.temp_val.length ; i++){
        name = $scope.temp_val[i].name +"\n"+ name;
        if(i==0)
          ic_array = $scope.temp_val[i].ic;
        else
          ic_array = ic_array+";"+$scope.temp_val[i].ic;
      }
      console.log(ic_array);
      openInfoModal("State Challenge","Select selection for State challenge?\n"+name).then(function () {
        $http.post("https://sir.mediaprimalabs.com/api/users/upgrade_to_state",
        { "auth_token": auth,
           "ics":ic_array
        })
        .then(function(response) {
            console.log(response);
            angular.forEach($scope.students, function(itm){ itm.selected = false; });
            $scope.showOptionStudent = false;
            $scope.reloadFromServer();
        });
        
      });
    };
    $scope.selectfinal = function() {
      $scope.temp_val = $filter('filter')($scope.students, {selected:true});
      var name = "",ic_array = "";
      for(var i=0;i < $scope.temp_val.length ; i++){
        name = $scope.temp_val[i].name +"\n"+ name;
        if(i==0)
          ic_array = $scope.temp_val[i].ic;
        else
          ic_array = ic_array+";"+$scope.temp_val[i].ic;
      }
      openInfoModal("Final Challenge","Select selection for Final challenge?\n"+name).then(function () {
        $http.post("https://sir.mediaprimalabs.com/api/users/upgrade_to_final",
        { "auth_token": auth,
           "ics":ic_array
        })
        .then(function(response) {
            console.log(response);
            angular.forEach($scope.students, function(itm){ itm.selected = false; });
            $scope.showOption = false;
            $scope.reloadFromServer();
        });
      });
    };
    $scope.addword = function() {
      openAddWordModal().then(function () {
        var f = document.getElementById('file').files[0],
        r = new FileReader();
        r.onloadend = function(e) {
          var values = [];
          angular.forEach(document.getElementById('file').files, function (item) {
              var extension = item.name.replace(/^.*\./, '').toLowerCase();
              var formData = new FormData();
              formData.append('audio',item);
              formData.append('auth_token',auth);

              var s = item.name.split(".");
              formData.append('word',s[0].toLowerCase());

              formData.append('level', $scope.word.level);
              formData.append('account_type',$scope.word.account_type);
              formData.append('education_type',$scope.word.education_type);
              formData.append('format',extension);
              $http({
                  url: "https://sir.mediaprimalabs.com/api/question_add",
                  method: "POST",
                  data: formData,
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
              }).success(function (response) {
                  //console.log(response);
                  if(response.success)
                    $scope.infoToast("Uploaded","Ref:"+response.ref);
                  else
                    $scope.dangerToast("Error",response);
                  $scope.reloadFromServer();
              }).error(function (response) {
                  console.log("error"+response);
              });
                  //values.push(value);
              });
          
        }
        r.readAsArrayBuffer(f);
        
      });
    };
    $scope.deleteWord = function() {
      $scope.temp_val = $filter('filter')($scope.words, {selected:true});
      var name = "";
      for(var i=0;i < $scope.temp_val.length ; i++){
        name = $scope.temp_val[i].word +"\n"+ name;
      }
      var del = confirm("Delete words?"+name);
      if(del){
        for(var i=0;i < $scope.temp_val.length ; i++){
          $http.post("https://sir.mediaprimalabs.com/api/question_delete/"+$scope.temp_val[i].id,
          { "auth_token": auth})
          .then(function(response) {
              console.log(response);
              $scope.dangerToast("Deleted",response.success);
              $scope.reloadFromServer();
          });
        }
        
      }
    };
    $scope.addsponsor = function() {
      openAddSponsorModal().then(function () {

        var f = document.getElementById('file_image').files[0],
        r = new FileReader();
        r.onloadend = function(e) {

              var formData = new FormData();
              formData.append('image',f);
              formData.append('auth_token',auth);
              formData.append('name',$scope.sponsor.name);

              $http({
                  url: "https://sir.mediaprimalabs.com/api/sponsors",
                  method: "POST",
                  data: formData,
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
                }).success(function (response) {
                    //console.log(response);
                    //if(response.success)
                    $scope.infoToast("Uploaded","Name:"+response.name);
                    //else
                    //  $scope.dangerToast("Error",response);
                    $scope.reloadFromServer();
                }).error(function (response) {
                  console.log("error"+response);
              });
          
        }
        r.readAsArrayBuffer(f);
      });
    };
    $scope.reloadpage = function(location) {
       $location.path("/"+location);
    };
    function openInfoModal(title,text) {
      var q = $q.defer(); //for callback
      var page = 'app/pages/main/widgets/infoModal.html';
      var size = 'md';
      $scope.modal_text = text;
      $scope.modal_title = title;
      $scope.onClickOK = function(){
        $scope.myModalInstance.close();
        q.resolve();
      }
      $scope.myModalInstance = $uibModal.open({
        animation: true,
        templateUrl: page,
        scope: $scope,
        controller: 'TablesPageCtrl',
        size: size,
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });
      return q.promise;
    };

    function openAddWordModal() {
      var q = $q.defer(); //for callback
      var page = 'app/pages/main/widgets/addWordModal.html';
      var size = 'md';
      $scope.word = {};
      $scope.levels = ["1", "2", "3"];
      $scope.account_type = ["student", "state", "final"];
      $scope.edu_type = ["primary", "secondary"];
      $scope.onClickOK = function(){
        console.log($scope.word);
        $scope.myModalInstance.close();
        q.resolve();
      }
      $scope.file_changed = function(element) {
          $scope.$apply(function(scope) {
              var file = element.files;
              var reader = new FileReader();
              reader.onload = function(e) {
                var w = "";
                angular.forEach(file, function (item) {
                   var s = item.name.split(".");
                   var value = {
                        name: item.name
                   };
                   w = w +","+ item.name;
                });
                $scope.word.word = w;
              };
              reader.readAsDataURL(file[0]);
          });
      };
      $scope.myModalInstance = $uibModal.open({
        animation: true,
        templateUrl: page,
        scope: $scope,
        controller: 'TablesPageCtrl',
        size: size,
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });
      return q.promise;
    };
    function openAddSponsorModal() {
      var q = $q.defer(); //for callback
      var page = 'app/pages/main/widgets/addSponsorModal.html';
      var size = 'md';
      $scope.sponsor = {};
      $scope.onClickOK = function(){
        $scope.myModalInstance.close();
        q.resolve();
      }
      $scope.file_changed = function(element) {
          $scope.$apply(function(scope) {
              var photofile = element.files[0];
              var reader = new FileReader();
              reader.onload = function(e) {
                  // handle onload
              };
              reader.readAsDataURL(photofile);
          });
      };
      $scope.myModalInstance = $uibModal.open({
        animation: true,
        templateUrl: page,
        scope: $scope,
        controller: 'TablesPageCtrl',
        size: size,
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });
      return q.promise;
    };
    
    $scope.$on('$destroy', function iVeBeenDismissed() {
      angular.extend(toastrConfig, defaultConfig);
    });
  }

})();
